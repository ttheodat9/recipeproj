var file = '';
window.onload = function () {
    $('#image').change(function (e) { //Event that is triggered when the file changes
        file = e.target.files[0]; //Variable that targets the file selected by the element selector (fileInput) 
        var imageType = /image.*/; //A variable to filter file type

        if (file.type.match(imageType)) { //It creates the instance of FileReader API only if the file type matches
            var reader = new FileReader(); //Creates the reader
            reader.readAsDataURL(file); //Reads the content of the file
            reader.onload = function (e) { //When the file finished loading, we can access the result
                $('#showImage').html(''); //Clears the DIV where the image will be displayed
                var img = new Image(); //Creates a new image
                img.src = reader.result; //Set the img src property using the data URL
                $('#showImage').append(img); //Add the image to the DIV
            };
        } else {
            $('#showImage').html('File not supported!'); //If the file selected is not supported, a message is displayed
        }
    });
};

$('#submitRecipe').click(() => {
    var recipeInfo = {
        name: '',
        instructions: '',
        picture: '',
        time: ''
    }
    recipeInfo.name = $('#recipeName').val();
    recipeInfo.instructions = $('#instructions').val();
    recipeInfo.time = $('#cookTime').val();
    if (recipeInfo.name == '' || recipeInfo.instructions == '' || recipeInfo.time == '') {
        swal('Invalid Recipe Submission', 'Please fill out the entire form correctly', 'error');
        return;
    }

    file = document.getElementById('image').files[0];//Reading image file
    if (file == null) {
        swal('Cannot find image', 'Please select an image', 'error');
        return;
    }
    var recipes = JSON.parse(localStorage.getItem('Recipes'));

    if (recipes == null)
        var recipes = [];
    var reader = new FileReader(); //Creates the reader
    reader.readAsDataURL(file); //Reads the content of the file
    reader.onload = function (e) { //When the file finished loading, we can access the result
        var img = $('#showImage'); //Creates a new image
        img.src = reader.result; //Set the img src property using the data URL
        recipeInfo.picture = img.src;

        recipes.push(recipeInfo);
        localStorage.setItem('Recipes', JSON.stringify(recipes));
        swal('Recipe Saved;)', 'Your recipe has been saved!', 'info');
    };
});

$('#newRecipe').click(() => {
    $('#recipeName').val('');
    $('#instructions').val('');
    $('#showImage').html('');
    $('#image').val('');
    $('#cookTime').val('');
    $('#recipeName').focus();
});

$('#showRecipes').click(() => {
    var recipes = JSON.parse(localStorage.getItem('Recipes'));
    var displayRecipes = '';
    $('#undoHidden').prop('hidden', false);

    if (recipes == null || recipes.length <= 0) {
        document.getElementById('recipeTable').innerHTML = 'There are no recipes to display:(';
    } else {
        var displayRecipes = `<thead>
        <tr>
            <th>Name</th>
            <th>Instructions</th>
            <th>Cook Time(min.)</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>`;
        for (let index = 0; index < recipes.length; index++) {
            displayRecipes += `<tr>
            <td><i class="fa fa-trash-o" style="color: orange" title="Delete Recipe" onclick=\"deleteRecipe(${index})\"></i> ${recipes[index].name} </td>
            <td> ${recipes[index].instructions} </td>
            <td> ${recipes[index].time} </td>
            <td><img src="${recipes[index].picture}" width="80" height="80" style="border-radius: 15px; border: groove">  </td>
            </tr>`;
        }
        $('#heading4').html('List of All Recipes');
        displayRecipes += '</tbody></table>';
        document.getElementById('recipeTable').innerHTML = displayRecipes;
    }
    // if (recipes <= 0){
    //     document.getElementById('recipeTable').innerHTML ='There is no data to display';
    // }
});

function deleteRecipe(index) {
    var recipes = JSON.parse(localStorage.getItem('Recipes'));
    swal({
        title: `Confirm Removal of the "${recipes[index].name}" Recipe`,
        text: 'Are you sure you want to delete this recipe?',
        buttons: {
            cancel: "Cancel",
            catch: {
                text: "Delete",
                value: "remove",
            },
        },
        icon: 'warning'
    }).then((value) => {
        switch (value) {
            case "remove":
                recipes.splice(index, 1);
                localStorage.setItem('Recipes', JSON.stringify(recipes));
                $('#showRecipes').click();
                break;
            default:
                $('#showRecipes').click();
        }
    });
}